#!/bin/env zsh

# Load colors and change shell ( taken from luke smith )
autoload -U colors && colors
PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "

# Load completion
autoload -U compinit
zstyle ':compinit:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)

# Load aliases
[ -f "${XDG_CONFIG_HOME:-HOME/.config}/shell/alias" ] && source "${XDG_CONFIG_HOME:-HOME/.config}/shell/alias"
