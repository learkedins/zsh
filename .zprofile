#!/usr/bin/env zsh

# change zshrc directory
export ZDOTDIR="$HOME/.config/shell"

# add stuff to path
export PATH="$HOME/.local/bin/:$PATH"

# cleaning up home directory
export XDG_CONFIG_HOME="$HOME/.config/"
export XDG_DATA_HOME="$HOME/.local/share"
